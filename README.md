# LemonwaySdkBundle: LemonWay SDK integration in Symfony

This bundle integrates [Lemon Way official PHP SDK](https://bitbucket.org/usul_/lemonway-php-sdk) in [the Symfony framework](http://symfony.com).

## Installation

Use [Composer](http://getcomposer.org) to install the bundle:

`composer require lemonway/sdk-bundle`

Then, update your `app/config/AppKernel.php` file:

```php
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Lemonway\SdkBundle\LemonwaySdkBundle(),
            // ...
        );

        return $bundles;
    }
```

Configure the bundle in `app/config/config.yml`:

```yaml
lemonway_sdk:
    directKitUrl: "%lemonway_directKitUrl%"
    webKitUrl:    "%lemonway_webKitUrl%"
    login:        "%lemonway_login%"
    password:     "%lemonway_password%"
    lang:         "%lemonway_lang%"
    debug:        "%lemonway_debug%"
```

Finally, update your `app/config/parameters.yml` file to store your Lemonway API credentials:

```yaml
parameters:
    # ...
    lemonway_directKitUrl: "MyDirectKitUrl"
    lemonway_webKitUrl:    "MyWebkitUrl"
    lemonway_login:        "MyLogin"
    lemonway_password:     "MyPassword"
    lemonway_lang:         "fr"
    lemonway_debug:        false
```

## Usage

The bundle automatically registers a `lemonway_sdk.api` service in the Dependency Injection Container. That service is
an instance of `\LemonWay\LemonWayAPI`.

Example usage in a controller:

```php
// ...

    public function registerWalletAction()
    {
        // Register a Wallet
        $res = $this->container->get('lemonway_sdk.api')
            ->RegisterWallet(array('wallet' => '123456789',
                        'clientMail' => '123456789@mail.fr',
                        'clientTitle' => 'U',
                        'clientFirstName' => 'John',
                        'clientLastName' => 'Doo'));

        if (isset($res->lwError)) {
            print 'Error, code '.$res->lwError->CODE.' : '.$res->lwError->MSG;
        } else {
            print '<br/>Wallet created : ' . $res->wallet->ID;

        }
    }

// ...
}
```

# For further use, please read the [Lemon Way official PHP SDK documentation](https://bitbucket.org/usul_/lemonway-php-sdk)


